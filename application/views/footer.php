<!-- Footer -->
<footer class="page-footer font-small bg-light">
	<!-- Copyright -->
	<div class="footer-copyright text-center py-3">© 2018 Copyright:
		<a href="https://mdbootstrap.com/education/bootstrap/"> CARIGEREJA.COM</a>
	</div>
	<!-- Copyright -->
</footer>
<!-- Footer -->
<script src="<?= base_url('assets/') ?>bootstrap4/js/jquery-3.3.1.min.js"></script>
<script src="<?= base_url('assets/') ?>bootstrap4/js/popper.min.js"></script>
<script src="<?= base_url('assets/') ?>bootstrap4/js/bootstrap.min.js"></script>
</body>
</html>