<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Home Page | AWP Website</title>
		<link rel="stylesheet" href="<?= base_url('assets/') ?>css/style.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/') ?>bootstrap4/css/bootstrap.min.css">
		<link rel="author" href="humans.txt">
		<style>
		</style>
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="#"><img src="<?= base_url('assets/') ?>images/brand.png" alt="brand" style="width:50px; height:50px;">GEREJAKU<span style="font-size:16px;">.COM</span></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Wilayah
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="#">Jakarta Utara</a>
							<a class="dropdown-item" href="#">Jakarta Pusat</a>
							<a class="dropdown-item" href="#">Jakarta Timur</a>
							<a class="dropdown-item" href="#">Jakarta Barat</a>
							<a class="dropdown-item" href="#">Jakarta Selatan</a>
						</div>
						<li class="nav-item">
							<a class="nav-link" href="#">Contact</a>
						</li>
					</li>
					<li class="nav-item">
						<a class="nav-link border border-secondary rounded" href="#" style="background:#7eadf7;">Sign-Up<span class="sr-only"></span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link border border-secondary rounded" href="#" style="background:#d2d8e0;">Login<span class="sr-only"></span></a>
					</li>
				</ul>
			</div>
		</nav>